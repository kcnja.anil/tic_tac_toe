"""This is a game of Tic-Tac-Toe
Player plays X and Computer plays O"""
import random
score_board=[0, 0]
player='X'
computer='O'
result=[]
layout = '   {}   |   {}   |   {}   \n-------|-------|-------\n   {}   |   {}   |   {}   \n-------|-------|-------\n   {}   |   {}   |   {}   '
board_layout=[0]


def check_result(value,board_is):                           # check for a win
    if board_is['a']==board_is['b']==board_is['c']==value:  # first horizontal win
        return True
    elif board_is['d']==board_is['e']==board_is['f']==value:  # second horizontal win
        return True
    elif board_is['g']==board_is['h']==board_is['i']==value:  # third horizontal win
        return True
    elif board_is['a']==board_is['d']==board_is['g']==value:  # first vertical win
        return True
    elif board_is['b']==board_is['e']==board_is['h']==value:  # second vertical win
        return True
    elif board_is['c']==board_is['f']==board_is['i']==value:   # third vertical win
        return True
    elif board_is['a']==board_is['e']==board_is['i']==value:  # diagonal win
        return True
    elif board_is['c']==board_is['e']==board_is['g']==value:  # diagonal win
        return True
    else:                                                     # no win
        return False


def play_game(play, a_dict):  # input the X's and O's
    new_layout=layout.format(a_dict['a'], a_dict['b'], a_dict['c'], a_dict['d'], a_dict['e'], a_dict['f'], a_dict['g'], a_dict['h'], a_dict['i'])
    if play=='X':
        a='PLAYER'
    else:
        a='COMPUTER'
    print(a+' plays: \n')
    print(new_layout)
    return new_layout


def take_input():     # take input from user and computer
    position={'a': 'a', 'b': 'b', 'c': 'c', 'd': 'd', 'e': 'e', 'f': 'f', 'g': 'g', 'h': 'h', 'i': 'i'}
    board_is={'a': 'a', 'b': 'b', 'c': 'c', 'd': 'd', 'e': 'e', 'f': 'f', 'g': 'g', 'h': 'h', 'i': 'i'}

    for game_round in range(1, 10):
        play_position = input('Enter your choice: ')
        position.pop(play_position)                   # remove the element from position dictionary
        board_is[play_position] = player              # add element to board_is dictionary
        n_l=play_game(player, board_is)               # return the modified layout of the game
        if not bool(position):                        # if there are no more options, game is over
            a=check_result(player, board_is)
            if not a:
                result.append('Tie')
                print('Tie!')
            else:
                score_board[0] += 1
                print('Score is: '+str(score_board[0]) + '\nPlayer wins')
                result.append('Player wins')
            break
        comp_position = random.choice(list(position.keys()))     # choose random element from position dictionary
        position.pop(comp_position)
        board_is[comp_position] = computer
        n_l=play_game(computer, board_is)
        if not bool(position):
            a=check_result(computer, board_is)
            if not a:
                result.append('Tie')
                print('Tie!')
            else:
                score_board[1] += 1
                print('Score is: '+str(score_board[1]) + '\nComputer wins')
                result.appends('Computer wins')
            break
        if game_round>=3:                                       # minimum requirement for a win
            a=check_result(player, board_is)                    # function to check for a win
            if a:
                score_board[0]+=1
                print('Score is: '+str(score_board[0])+'\nPlayer wins')
                result.append('Player wins')
                break
            else:
                a=check_result(computer, board_is)
            if a:
                score_board[1]+=1
                print('Score is: '+str(score_board[1])+'\nComputer wins')
                result.append('Computer wins')
                break
            else:
                continue
    board_layout.append(n_l)


for game in range(1, 11):
    print('GAME: '+str(game))
    print('This is the layout of the game:\n')        # the layout of the game
    print('  a  |  b  |  c  ')
    print('-----|-----|-----')
    print('  d  |  e  |  f  ')
    print('-----|-----|-----')
    print('  g  |  h  |  i  \n')
    print('Player is X, Computer is O')
    take_input()
if score_board[0]==score_board[1]:                   # printing the result
    print('!Game is a Tie!')
elif score_board[0]>score_board[1]:
    print('!Player wins the game!')
else:
    print('!Computer wins the game!')
while True:
    ask = input('Want to know round details:y/n ')
    if ask == 'n':
        exit()
    game_i = input('Enter the round number: ')         # ask user for input
    print('The board layout is:  \n' + board_layout[int(game_i)])
    print('Player score is: ' + str(score_board[0]) + '\nComputer score is: ' + str(score_board[1]))
    print('Game Result for this round is:  ' + result[int(game_i) - 1])



